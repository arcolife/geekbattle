from django.db import models
from django.contib.auth.models import User
from datetime import datetime

# Create your models here.
class Post(models.Model):
    title = models.CharField(blank=False, null=False, maxlength=2048)
    author = models.ForeignKey(User, related_name='posts')
    created = models.DateTimeField(u'Date Created', blank = False, null= False, default=datetime.now)
    slug = models.SlugField(prepopulate_from=("title",))
    
    def __str__(self):
        return self.title
    
class PostItem(models.Model):
    post = models.ForeignKey(Post, related_name='posts')
    created = models.DateTimeField(u'Date Created', blank = False, default=datetime.now)
    image = models.ImageField(upload_to="uploads", blank=True)
    content = models.TextField(blank=True)
    
    def __str__(self):
        return "Post item for '%s' created at %s" % (self.post.title, self.created.strftime('%I:%M %p %Z'))
