from django.db import models
from django.forms import ModelForm
# Create your models here.
class Correct(models.Model):
    qno = models.CharField(max_length=20)
    question = models.CharField(max_length=700)
    answer = models.CharField(max_length=200)

class User(models.Model):
    uid = models.CharField(max_length=20)
    qno = models.CharField(max_length=20)
    answer = models.CharField(max_length=20)

class UserForm(ModelForm):
    class Meta:
        model = User
class Final(models.Model):
    answer = models.CharField(max_length=200)

class FinalForm(ModelForm):
    class Meta:
        model = Final
