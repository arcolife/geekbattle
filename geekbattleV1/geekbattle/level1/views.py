# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpRequest
from django.template import Context, loader
from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from geekbattle.level1.models import UserForm
from geekbattle.level1.models import FinalForm
from django.template import RequestContext
from geekbattle.userinfo.models import userlev1
from django.db.models import F
class AboutView(TemplateView):
    template_name = "thanks.html"

class QuesCorrect(TemplateView):
    template_name = "correct.html"
class QuesIncorrect(TemplateView):
    template_name = "incorrect.html"
class sorry(TemplateView):
    template_name = "sorry.html"



from django import forms
# import all question in variable
from level1.models import Correct

#for getting session
def sess(request):
    try:
        uid = int(request.session['kantesh'])
        uid=uid+1
        return uid
    except KeyError:
        uid = -1
        return uid

def ques(request):
    if request.method == 'GET':
# print question according to request
        qno=int(request.GET.get('qno'))
        ques=Correct.objects.get(qno=qno)
        a=ques.question
        w=FinalForm()
        uid=sess(request)
        if uid == -1 :
            return HttpResponseRedirect('/sorry/')
        else:
             userf=userlev1.objects.get(id=uid)
             var2='k = userf.x'+str(qno)
             exec var2
             if k == "":
                 return render_to_response("question_form.html",{"a": a,"qno":qno,"w":w,"uid":uid},context_instance=RequestContext(request),)
             else:
                 userf.attemp=int(userf.attemp)+1
                 userf.save()
                 err='Donot try to cheat in event.Your attemp is increased by one for this attempt'
                 return render_to_response("question_form.html",{"a": a,"qno":qno,"w":w,"uid":uid,"err":err},context_instance=RequestContext(request),)
    else:
        if request.method == 'POST':
            qno=int(request.GET.get('qno'))
            a = FinalForm(request.POST)
            if a.is_valid():
                y = a.cleaned_data
# for matching answer
                ques=Correct.objects.get(qno=qno)
                if (ques.answer==y['answer']):
                    #for saving data to file
                    uid = sess(request)
                    if uid == -1 :
                        return HttpResponseRedirect('/sorry/')
                    else:
                        uid = int(uid)
                        userf=userlev1.objects.get(id=uid)
                #for updating score and attempt
                        var='userf.x'+str(qno)+'=1'
                        sc=int(userf.score)+1
                        att=int(userf.attemp)+1
                        userf.attemp=att
                        userf.score = sc
                        exec var
                        userf.save()
                        return HttpResponseRedirect('/correct/')
                else:
                    uid = sess(request)
                    if uid == -1 :
                        return HttpResponseRedirect('/sorry/')
                    else:
                        uid = int(uid)
                        userf=userlev1.objects.get(id=uid)
                        att=int(userf.attemp)+1
                        userf.attemp=att
                        userf.save()
                        return HttpResponseRedirect('/incorrect/')
