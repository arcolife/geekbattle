from level1.models import Correct
from django.contrib import admin
from register.models import Register
from userinfo.models import userlev1
admin.site.register(Correct)
admin.site.register(Register)
admin.site.register(userlev1)
