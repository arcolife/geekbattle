from django.conf.urls.defaults import patterns, include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from level1.views import AboutView
from level1.views import QuesCorrect
from level1.views import QuesIncorrect
from level1.views import sorry
from level1.views import ques
from userinfo.views import lev1form
from register.views import regis
from login.views import login
from login.views import profile
from login.views import welcome
from login.views import notregis
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'geekbattle.views.home', name='home'),
    # url(r'^geekbattle/', include('geekbattle.foo.urls')),
   
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^thanks/',AboutView.as_view()),
     url(r'^correct/',QuesCorrect.as_view()),
     url(r'^incorrect/',QuesIncorrect.as_view()),
     url(r'^sorry/',sorry.as_view()),
     url(r'^welcome/',welcome.as_view()),
     url(r'^login/',login),
     url(r'^register/',regis),
     url(r'^question/',ques),
     url(r'^level1/',lev1form),
     url(r'^profile/',profile),
     url(r'^loginerror/',notregis.as_view())
)
