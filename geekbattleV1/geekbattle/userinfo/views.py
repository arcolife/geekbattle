# Create your views here.
from django.http import HttpResponseRedirect
from django.template import Context, loader
from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from django.template import RequestContext
from geekbattle.userinfo.models import userlev1
from django.db.models import F
from django import forms 
from itertools import izip

#creating array
def nouser(request):
    a=[]
    b=[]
    try:
        uid=int(request.session['kantesh'])+1
    except KeyError:
        return a,b
    else:
        userf=userlev1.objects.get(id=uid)
        for i in range(1,51,1):
            a.append(i)
            var='k = userf.x'+str(i)
            exec var
            if k == "":
                b.append(1)
            else:
                b.append(2)
        return a,b

def lev1form(request):
    a,b=nouser(request)
    c = zip(a, b)
    if c ==[]:
        return HttpResponseRedirect('/sorry/')
    else:
        return render_to_response('level1.html',{"c":c}, context_instance=RequestContext(request),)

