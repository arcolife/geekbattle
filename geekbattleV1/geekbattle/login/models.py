from django.db import models

# Create your models here.
from django.forms import ModelForm
from django import forms
class LogInForm(forms.Form):
    Email = forms.EmailField(max_length=50, label="Email id")
    Password = forms.CharField(widget=forms.PasswordInput, label="Password")
