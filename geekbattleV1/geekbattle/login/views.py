# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import Context, loader
from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from geekbattle.login.models import LogInForm
from django.template import RequestContext
from register.models import Register
from geekbattle.userinfo.models import userlev1
from django.db.models import F
from django.http import Http404
            
class notregis(TemplateView):
    template_name = "notregistered.html" 

def sessi(request):
    try:
        uid = int(request.session['kantesh'])
        uid=uid+1
        return uid
    except KeyError:
        uid = -1
        return uid

v = Register.objects.all()
class welcome(TemplateView):
    template_name = "dashboard.html"

def login(request):
    try:
        if request.method == 'POST':
            form = LogInForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                try:
                    user=cd['Email']
                    k=Register.objects.get(Email=user)
                except Register.DoesNotExist:
                    return HttpResponseRedirect('/loginerror/')
                else:
                    if k.Password == cd["Password"]:
                        request.session['kantesh'] = (k.id)-1
                        sess=request.session['kantesh']
                        uid = sessi(request)
                        userf=userlev1.objects.get(id=uid)
                        score=userf.score
                        attemp=userf.attemp
                        return render_to_response('dashboard.html',{'sess':sess,'name':k.Name,"score":score,"attemp":attemp},context_instance=RequestContext(request))
                    else:
                        return HttpResponseRedirect('/login/')
    except ValueError:
        return HttpResponseRedirect('/login/')
    else:
            form = LogInForm()
            return render_to_response('login.html',{
                    "form": form},context_instance=RequestContext(request))

def profile(request):
    uid = sessi(request)
    if uid == -1 :
        return HttpResponseRedirect('/sorry/')
    else:
        k=Register.objects.get(id=uid)
        userf=userlev1.objects.get(id=uid)
        score=userf.score
        attemp=userf.attemp
        return render_to_response('dashboard.html',{'name':k.Name,"score":score,"attemp":attemp},context_instance=RequestContext(request))

