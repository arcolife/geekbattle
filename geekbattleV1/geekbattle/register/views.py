# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import Context, loader
from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from geekbattle.register.models import RegisterForm
from django.template import RequestContext
from geekbattle.userinfo.models import userlev1Form
from django.db.models import F
from geekbattle.register.models import Register
import time
from django.core.mail import send_mail
def regis(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            em=cd['Email']
            userda=userlev1Form({'attemp' : 0 , 'score' : 0  })
            try:
                k=Register.objects.get(Email=em)
            except Register.DoesNotExist:
            #send_mail('your password', 'test mail', 'kanteshraj@live.in',
             #         ['kantesh555@gmail.com'])
                form.save()
                time.sleep(0.01)
                userda.save()
                return HttpResponseRedirect('/thanks/')
            else:
                err="You are already registered by this email id"
                return render_to_response('register.html',{
                        "form":form,"err":err},context_instance=RequestContext(request))
        else:
            return HttpResponseRedirect('/register/')
    else:
        form = RegisterForm()
        return render_to_response('register.html',{
                "form": form},context_instance=RequestContext(request))

