from django.db import models
from django.forms import ModelForm
from django.forms import Textarea

                                                                                                                                     
class Register(models.Model):
    Name = models.CharField(max_length=50)
    Email = models.EmailField()
    Password = models.CharField(max_length=20)
    Qualification = models.CharField(max_length=20)
    Institution = models.CharField(max_length=30)
    Address = models.CharField(max_length=300)
    Pincode = models.IntegerField(max_length=10)
    
    

class RegisterForm(ModelForm):
    class Meta:
        model = Register
        fields = ('Name', 'Email', 'Qualification', 'Institution', 'Address', 'Pincode')
        widgets = {
            'Address': Textarea(attrs={'cols': 100, 'rows': 5}),
        }
