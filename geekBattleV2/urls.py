from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from level3.views import save_db,index,submitted

urlpatterns = patterns('',
    url(r'^$',index.as_view(), name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^submit/',save_db),
    url(r'^submitted/',submitted.as_view(),name='submitted')
            
    # Examples:
    # url(r'^$', 'geekBattleV2.views.home', name='home'),
    # url(r'^geekBattleV2/', include('geekBattleV2.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    
)
